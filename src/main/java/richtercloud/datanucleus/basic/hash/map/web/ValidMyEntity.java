package richtercloud.datanucleus.basic.hash.map.web;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author richter
 */
@Target( { ElementType.TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = MyEntityValidator.class)
@Documented
public @interface ValidMyEntity {

    String message() default "invalid dimension usage stats";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
