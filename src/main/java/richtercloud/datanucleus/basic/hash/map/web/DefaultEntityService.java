package richtercloud.datanucleus.basic.hash.map.web;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author richter
 */
@Stateless
public class DefaultEntityService implements EntityService {
    @PersistenceContext
    private EntityManager entityManager;

    public DefaultEntityService() {
    }

    @Override
    public MyEntity getOnlyMyEntity() {
        MyEntity retValue = entityManager.find(MyEntity.class,
                1l);
        if(retValue == null) {
            retValue = new MyEntity();
            entityManager.persist(retValue);
        }
        return retValue;
    }
}
