package richtercloud.datanucleus.basic.hash.map.web;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import javax.measure.unit.Unit;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang3.tuple.MutablePair;

/**
 *
 * @author richter
 */
@Entity
@ValidMyEntity
@XmlRootElement
public class MyEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull
    @Id
    @GeneratedValue
    private Long id = 1l;
    @NotNull
    @Basic
    @Column(length=255*8)
    private HashMap<Unit<?>, MutablePair<Integer, Date>> usageMap = new HashMap<>();

    public MyEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HashMap<Unit<?>, MutablePair<Integer, Date>> getUsageMap() {
        return usageMap;
    }

    public void setUsageMap(HashMap<Unit<?>, MutablePair<Integer, Date>> usageMap) {
        this.usageMap = usageMap;
    }
}
