package richtercloud.datanucleus.basic.hash.map.web;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author richter
 */
public class MyEntityValidator extends AbstractMyEntityValidator implements ConstraintValidator<ValidMyEntity, MyEntity> {

    @Override
    public void initialize(ValidMyEntity constraintAnnotation) {
        //do nothing
    }

    @Override
    public boolean isValid(MyEntity value, ConstraintValidatorContext context) {
        boolean retValue = validateUsageMap(value.getUsageMap());
        return retValue;
    }
}
