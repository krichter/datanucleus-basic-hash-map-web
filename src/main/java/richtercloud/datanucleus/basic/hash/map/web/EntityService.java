package richtercloud.datanucleus.basic.hash.map.web;

import javax.ejb.Local;

/**
 *
 * @author richter
 */
@Local
public interface EntityService {

    MyEntity getOnlyMyEntity();
}
