package richtercloud.datanucleus.basic.hash.map.web;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author richter
 */
@Named
@ViewScoped
public class MyManagedBean implements Serializable {
    private static final long serialVersionUID = 1L;
    @EJB
    private EntityService entityService;

    public MyManagedBean() {
    }

    public String myAction() {
        MyEntity myEntity = entityService.getOnlyMyEntity();
        System.out.println("myAction executed: "+myEntity);
        return "";
    }
}
